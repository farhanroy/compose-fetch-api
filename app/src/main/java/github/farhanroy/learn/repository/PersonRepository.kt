package github.farhanroy.learn.repository

import com.github.kittinunf.fuel.Fuel
import com.github.kittinunf.fuel.core.FuelError
import com.github.kittinunf.fuel.core.Request
import com.github.kittinunf.fuel.core.requests.tryCancel
import com.github.kittinunf.fuel.gson.responseObject
import github.farhanroy.learn.models.Data
import github.farhanroy.learn.models.Person
import github.farhanroy.learn.models.PersonDetail
import javax.inject.Singleton

@Singleton
class PersonRepository {
    private val BASE_URL = "https://reqres.in/api"
    private lateinit var requestGallery: Request

    fun getPersons(onSuccess:(Person) -> Unit, onError:(FuelError) -> Unit) {
        requestGallery = Fuel.get("$BASE_URL/users?page=1").responseObject<Person> { _, _, result ->
            result.fold({
                onSuccess(it)
            }, {
                onError(it)
            })
        }
    }

    fun getPerson(id: Int, onSuccess:(Data) -> Unit, onError:(FuelError) -> Unit) {
        requestGallery = Fuel.get("$BASE_URL/users/$id").responseObject<PersonDetail> { _, _, result ->
            result.fold({
                onSuccess(it.data)
            }, {
                onError(it)
            })
        }
    }

    fun cleared() {
        requestGallery.tryCancel(true)
    }
}