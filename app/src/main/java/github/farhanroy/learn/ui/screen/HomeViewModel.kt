package github.farhanroy.learn.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import github.farhanroy.learn.models.Person
import github.farhanroy.learn.repository.PersonRepository
import javax.inject.Inject

@HiltViewModel
class HomeViewModel @Inject constructor(private val repository: PersonRepository): ViewModel() {
    private  var _person = MutableLiveData<Person>()
    val person: LiveData<Person?> = _person


    fun requestPersons(){
        repository.getPersons({
            _person.postValue(it)
        },{})
    }

    override fun onCleared() {
        super.onCleared()
        repository.cleared()
    }
}
