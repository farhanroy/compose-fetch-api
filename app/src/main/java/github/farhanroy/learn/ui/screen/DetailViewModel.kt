package github.farhanroy.learn.ui.screen

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import github.farhanroy.learn.models.Data
import github.farhanroy.learn.repository.PersonRepository
import javax.inject.Inject

@HiltViewModel
class DetailViewModel @Inject constructor(private val repository: PersonRepository) : ViewModel() {
    private var _person = MutableLiveData<Data>()
    val person: LiveData<Data?> = _person


    fun requestPerson(id: Int) {
        repository.getPerson(id = id, {
            _person.postValue(it)
        }, {})
    }

    override fun onCleared() {
        super.onCleared()
        repository.cleared()
    }
}