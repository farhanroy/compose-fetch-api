package github.farhanroy.learn.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.size
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.coil.rememberCoilPainter
import github.farhanroy.learn.models.Data

@Composable
fun DetailScreen(backToHome: () -> Unit, id: Int){
    val viewModel = hiltViewModel<DetailViewModel>()
    viewModel.requestPerson(id)
    val person: Data? by viewModel.person.observeAsState()
    Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text("Detail Person")
                },
                navigationIcon = {
                    IconButton(onClick = { backToHome() }) {
                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = null)
                    }
                }
            )
        },
    ) {
        DetailView(person)
    }
}

@Composable
fun DetailView(person: Data?) {
    Column(
        horizontalAlignment = Alignment.CenterHorizontally
    ) {
        Image(
            modifier = Modifier.size(height = 256.dp, width = 256.dp),
            painter = rememberCoilPainter(person?.avatar),
            contentDescription = ""
        )
        Text("${person?.first_name} ${person?.last_name}")
        Text("${person?.email}")
    }
}