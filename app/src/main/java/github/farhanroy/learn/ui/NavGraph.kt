package github.farhanroy.learn.ui

import androidx.activity.compose.BackHandler
import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.lifecycle.Lifecycle
import androidx.navigation.NavBackStackEntry
import androidx.navigation.NavHostController
import androidx.navigation.NavType
import androidx.navigation.compose.*
import github.farhanroy.learn.ui.MainDestinations.DETAIL_ID_KEY
import github.farhanroy.learn.ui.MainDestinations.DETAIL_ROUTE
import github.farhanroy.learn.ui.MainDestinations.HOME_ROUTE
import github.farhanroy.learn.ui.screen.DetailScreen
import github.farhanroy.learn.ui.screen.HomeScreen

/**
 * Destinations used in the App.
 */
object MainDestinations {
    const val HOME_ROUTE = "home"
    const val DETAIL_ROUTE = "detail"

    const val DETAIL_ID_KEY = "personId"
}

@Composable
fun NavGraph(
    finishActivity: () -> Unit = {},
    navController: NavHostController = rememberNavController(),
    startDestination: String = HOME_ROUTE
) {
    val actions = remember(navController) { MainActions(navController) }

    NavHost(
        navController = navController,
        startDestination = startDestination
    ) {
        composable(HOME_ROUTE) {
            // Intercept back in Home: make it finish the activity
            BackHandler {
                finishActivity()
            }
            HomeScreen(toDetail = {
                actions.detailScreen(it)
            })
        }
        composable(
            "${MainDestinations.DETAIL_ROUTE}/{$DETAIL_ID_KEY}",
            arguments = listOf(
                navArgument(DETAIL_ID_KEY) { type = NavType.IntType }
            )
        ) { backStackEntry: NavBackStackEntry ->
            val arguments = requireNotNull(backStackEntry.arguments)
            val currentPersonId = arguments.getInt(DETAIL_ID_KEY, 0)
            DetailScreen(
                id = currentPersonId,
                backToHome = { actions.upPress() },
            )
        }
    }
}

/**
 * Models the navigation actions in the app.
 */
class MainActions(navController: NavHostController) {
    val homeScreen: () -> Unit = {
        navController.navigate(HOME_ROUTE)
    }
    val detailScreen: (Int) -> Unit = { id ->
        navController.navigate("$DETAIL_ROUTE/$id")
    }
    val upPress: () -> Unit = {
        navController.navigateUp()
    }
}

/**
 * If the lifecycle is not resumed it means this NavBackStackEntry already processed a nav event.
 *
 * This is used to de-duplicate navigation events.
 */
private fun NavBackStackEntry.lifecycleIsResumed() =
    this.lifecycle.currentState == Lifecycle.State.RESUMED