package github.farhanroy.learn.ui.screen

import androidx.compose.foundation.Image
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.material.Scaffold
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import com.google.accompanist.coil.rememberCoilPainter
import github.farhanroy.learn.models.Data
import github.farhanroy.learn.models.Person

@Composable
fun HomeScreen(toDetail: (id: Int) -> Unit) {
    val viewModel = hiltViewModel<HomeViewModel>()
    viewModel.requestPersons()
    val persons: Person? by viewModel.person.observeAsState()
    return Scaffold(
        topBar = {
            TopAppBar(
                title = {
                    Text("Person")
                },
            )
        },
    ) {
        PersonList(persons = persons?.data, toDetail = toDetail)
    }
}

@Composable
fun PersonList(persons: List<Data>?, toDetail: (id: Int) -> Unit) {
    Column {
        persons?.forEach { person ->
            ItemList(person, toDetail)
        }
    }
}

@Composable
fun ItemList(person: Data, toDetail: (id: Int) -> Unit) {
    return Row(modifier = Modifier.padding(8.dp).clickable {
        toDetail(person.id)
    }) {
        Image(
            modifier = Modifier.size(height = 48.dp, width = 48.dp),
            painter = rememberCoilPainter(person.avatar),
            contentDescription = ""
        )
        Spacer(modifier = Modifier.width(8.dp))
        Column {
            Text("${person.first_name} ${person.last_name}")
            Text(person.email)
        }
    }
}