package github.farhanroy.learn.di

import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import github.farhanroy.learn.repository.PersonRepository
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object AppProviders {
    @Provides
    @Singleton
    fun providePersonRepository() = PersonRepository()
}